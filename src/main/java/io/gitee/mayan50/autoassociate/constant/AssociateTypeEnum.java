package io.gitee.mayan50.autoassociate.constant;

/**
 * 关联关系类型枚举
 */
public enum AssociateTypeEnum {

    ONE_TO_ONE,
    ONE_TO_MANY

}
