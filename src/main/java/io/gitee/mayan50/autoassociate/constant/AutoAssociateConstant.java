package io.gitee.mayan50.autoassociate.constant;

import java.util.concurrent.TimeUnit;

/**
 * 常量类
 */
public class AutoAssociateConstant {

    public final static String DEFAULT_GROUP_NAME = "DEFAULT";

    public final static long DEFAULT_TIMEOUT = 30;

    public final static TimeUnit DEFAULT_TIMEOUT_UNIT = TimeUnit.MINUTES;

    public final static int BASE_AUTO = 1;

    public final static int USER_AUTO = 2;

}
