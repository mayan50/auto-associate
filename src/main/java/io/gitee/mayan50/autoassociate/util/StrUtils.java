package io.gitee.mayan50.autoassociate.util;

/**
 * 字符串工具类
 */
public class StrUtils {

    public static boolean isBlock(String str) {
        return str == null || str.length() == 0 || str.trim().length() == 0;
    }

    public static boolean isNotBlock(String str) {
        return !isBlock(str);
    }

    /**
     * 下划线转驼峰
     * @param str 下划线变量
     * @return 驼峰变量
     */
    public static String humpToLine(String str) {
        StringBuilder sb = new StringBuilder();
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if(chars[i] == '_' && i < chars.length - 1) sb.append(Character.toUpperCase(chars[++i]));
            else sb.append(chars[i]);
        }
        return sb.toString();
    }

}
