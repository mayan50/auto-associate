package io.gitee.mayan50.autoassociate.config;

import io.gitee.mayan50.autoassociate.aop.AutoAssociateAop;
import io.gitee.mayan50.autoassociate.auto.AutoAssociateManager;
import io.gitee.mayan50.autoassociate.auto.impl.AutoAssociateManagerImpl;
import io.gitee.mayan50.autoassociate.util.DBUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动配置类
 */
@Configuration
public class AutoAssociateConfig {

    @ConditionalOnMissingBean(AutoAssociateManager.class)
    @Bean
    public AutoAssociateManager autoAssociate() {
        return new AutoAssociateManagerImpl();
    }

    @Bean
    public DBUtils dbUtils() {
        return new DBUtils();
    }

    @Bean
    public AutoAssociateAop autoAssociateAop() {
        return new AutoAssociateAop();
    }

}
