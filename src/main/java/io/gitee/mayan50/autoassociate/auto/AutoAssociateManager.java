package io.gitee.mayan50.autoassociate.auto;

import io.gitee.mayan50.autoassociate.annotation.AutoAssociate;

import java.util.Collection;

/**
 * 关联关系关联类 接口 方便扩展实现
 */
public interface AutoAssociateManager {

    Object autoAssociateObj(Object obj, AutoAssociate autoAssociate);

    <T> T autoAssociate(T entity, AutoAssociate autoAssociate);

    <T> Collection<T> autoAssociate(Collection<T> list, AutoAssociate autoAssociate);

    default boolean isBaseEnable() {
        return false;
    }

    default boolean isEntity(String className) {
        return true;
    }

}
