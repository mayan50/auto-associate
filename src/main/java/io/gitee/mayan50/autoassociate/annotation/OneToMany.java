package io.gitee.mayan50.autoassociate.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 一对多注解
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OneToMany {

    /**
     * mapper class
     * @return mapper class
     */
    Class<?> mapperEntity() default void.class;

    /**
     * 本类关联字段（数据库字段名）
     * @return 本类关联字段（数据库字段名）
     */
    String column() default "";

    /**
     * 关联类关联字段（数据库字段名）
     * @return 关联类关联字段（数据库字段名）
     */
    String associateColumn() default "";

    /**
     * 暂时废弃
     * @return 暂时废弃
     */
    @Deprecated
    int deep() default 1;

    /**
     * 自定义sql
     * @return 自定义sql
     */
    String sql() default "";

    /**
     * 自定义sql参数
     * @return 自定义sql参数
     */
    String[] condition() default {};

}
