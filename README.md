## Mybatis-Plus 一对多插件
> 受[MPRelation](https://gitee.com/dreamyoung/mprelation.git)启发编写

> [源码](https://gitee.com/mayan50/auto-associate)

[//]: # (> [示例]&#40;https://gitee.com/mayan50/auto-associate-demo&#41;)
### Maven
```
<dependency>
    <groupId>io.gitee.mayan50</groupId>
    <artifactId>autoassociate</artifactId>
    <version>1.3</version>
</dependency>
```
### 注解
###### @OneToMany 一对多注解
###### @OneToOne 一对一注解
> 参数
- targetEntity 对应的Mapper class
- column 本类关联的字段（数据库的字段名称）
- associateColumn 对应关联的字段（数据库的字段名称）
- table 表名 （暂时没啥用）
- deep 关联深度
- sql 自定义sql（如有自定义sql，将不使用关联关系）
- condition 自定义sql参数
### 实体类（使用）
```java
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.my.autoassociate.annotation.OneToMany;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ToString
public class Pirates {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    private Long captain;
    private String remark;

    @TableField(exist = false)
    @OneToMany(column = "id", associateColumn = "pirates_id")
    List<User> users;

}
```
```java
@Data
@Accessors(chain = true)
@ToString
public class User {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    private Integer age;
    private Long piratesId;

    @TableField(exist = false)
    @OneToOne(column = "pirates_id", associateColumn = "id")
    private Pirates pirates;
}
```

```java
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import OneToMany;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ToString
public class Pirates {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    private Long captain;
    private String remark;

    @TableField(exist = false)
    @OneToMany(column = "id", associateColumn = "pirates_id")
    List<User> users;

}
```
```java
@Data
@Accessors(chain = true)
@ToString
public class User {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    private Integer age;
    private Long piratesId;

    @TableField(exist = false)
    // @OneToOne(column = "pirates_id", associateColumn = "id")
    // 形式如此，a:pirates_id 实际执行时会取当前对象的piratesId作为最终参数查询
    @OneToOne(sql = "SELECT * FROM `pirates` WHERE id = ?", condition = "a:pirates_id")
    private Pirates pirates;

}
```

###### @AutoAssociate
自定义方法只需要加上此注解（Service接口中）可自动完成一对一和一对多关联

```java
/**
 * 自动关联配置类（非必须）
 */
@Configuration
public class AutoAssociateConfig {

    /**
     * 自动关联Bean配置，非必须，可使用默认实现，也可自定义实现
     * @return 自动关联Bean
     */
    @Bean
    public AutoAssociateManager autoAssociateManager() {
        // 第一个参数为IService中接口是否默认关联 第二个参数为实体类包名
        return new AutoAssociateManagerImpl(true, "com.my.autoassociate.entity");
    }

}
```

```java
import com.baomidou.mybatisplus.extension.service.IService;
import com.my.autoassociate.entity.User;

public interface UserService extends IService<User> {
    User findById(Long id);
}
```

```java
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Override
    @AutoAssociate
    public User findById(Long id) {
        return baseMapper.findById(id);
    }

}
```
配置好bean后原生的方法会自动关联

> 如果有使用的小伙伴有好的意见尽管提出，我可能会修改了